#ifndef FILEREADER_H
#define FILEREADER_H

#include <QObject>
#include <QFile>
#include "datacontainer/dc_definitions.h"
#include "dc_definitions.h"

class FileReader : public QObject
{
    Q_OBJECT

    QString parseExtendedSwitchData(int switchType, int switchDirectionType, int switchFixedDirection, int deviceDirection, int deviceSide, const QString& switchLabel);
    std::map<int, QString> extendedSwitchDictionary{
        {static_cast<int>(SwitchType::SingleSwitchContiniousRollingSurface), QObject::tr("SCRS")},
        {static_cast<int>(SwitchType::CrossSwitchContiniousRollingSurface), QObject::tr("SCRS")},
        {static_cast<int>(SwitchType::SingleSwitch), QObject::tr("S")},
        {static_cast<int>(SwitchType::CrossSwitch), QObject::tr("S")},
        {static_cast<int>(SwitchType::DropSwitch), QObject::tr("S")},
        {static_cast<int>(SwitchType::OtherSwitch), QObject::tr("S")},
        {static_cast<int>(SwitchDirectionType::Left), QObject::tr("L")},
        {static_cast<int>(SwitchDirectionType::Right), QObject::tr("R")},
        {static_cast<int>(SwitchDirectionType::SingleThreadLeft), QObject::tr("1Th-L")},
        {static_cast<int>(SwitchDirectionType::SingleThreadRight), QObject::tr("1Th-R")},
        {static_cast<int>(SwitchDirectionType::DualThreadLeft), QObject::tr("2Th-L")},
        {static_cast<int>(SwitchDirectionType::DualThreadRight), QObject::tr("2Th-R")},
        {static_cast<int>(SwitchFixedDirection::Plus), "+"},
        {static_cast<int>(SwitchFixedDirection::Minus), "-"},
        {static_cast<int>(SwitchFixedDirection::LeftPlus), QObject::tr("L+")},
        {static_cast<int>(SwitchFixedDirection::LeftMinus), QObject::tr("L-")},
        {static_cast<int>(SwitchFixedDirection::RightPlus), QObject::tr("R+")},
        {static_cast<int>(SwitchFixedDirection::RightMinus), QObject::tr("R-")},
        {static_cast<int>(DeviceDirection::AgainstWool), QObject::tr("AW")},
        {static_cast<int>(DeviceDirection::ByWool), QObject::tr("BW")},
        {static_cast<int>(DeviceDirection::LeftPlus), QObject::tr("L+")},
        {static_cast<int>(DeviceDirection::LeftMinus), QObject::tr("L-")},
        {static_cast<int>(DeviceDirection::RightPlus), QObject::tr("R+")},
        {static_cast<int>(DeviceDirection::RightMinus), QObject::tr("R-")},
        {static_cast<int>(DeviceDirection::AgainstWoolPlus), QObject::tr("AW+")},
        {static_cast<int>(DeviceDirection::AgainstWoolMinus), QObject::tr("AW-")},
        {static_cast<int>(DeviceDirection::ByWoolPlus), QObject::tr("BW+")},
        {static_cast<int>(DeviceDirection::ByWoolMinus), QObject::tr("BW-")},
    };

public:
    explicit FileReader(sFileHeader Head);
    void fileReaderVectorInit(std::vector<std::pair<int, int>> coordVector);
    int firstFileRead(QFile* file);
    void addEvent(unsigned char id, QString name);
    void addEvent(unsigned char id);
    void setSens(int difference, stateTune setOfKu, stateTune setOfstStr, stateTune setOfendStr);
    bool prepareForReadSpan(int minCoord, int maxCoord, BScanDisplaySpan& span, QFile* file);
    bool readWhileWriteSpan(int StartId, int minCoord, int EndDis, QFile* file, BScanDisplaySpan& span);
    int prepare(int startDist, bool save, QFile* file);
    inline bool getEventSpan(eventSpan& span)
    {
        span = _eventData;
        return true;
    }
    int getMaxDisCoord()
    {
        return _maxDisCoord;
    }
    void distanceCalculation(int& km, int& pk, int& metre, int disCoord, int sysCoord, int& direct);
    void sensCalculation(std::vector<std::vector<unsigned char>>& sens, std::vector<stateTune> vector, int disCoord);
    void delegateSensToModel(int disCoord, std::vector<std::vector<unsigned char>>& kuSens, std::vector<std::vector<unsigned char>>& stStrSens, std::vector<std::vector<unsigned char>>& endStrSens);
signals:

public slots:
private:
    std::array<std::array<allSens, 16>, 3> _railChanSens;
    std::vector<std::pair<int, int>> _coordVector;
    std::vector<std::array<int, 5>> _coordinateMarksVector;
    std::vector<stateTune> _KUChangeCoord;
    std::vector<stateTune> _stStrChangeCoord;
    std::vector<stateTune> _endStrChangeCoord;
    eventFileItem _event;
    eventSpan _eventData;
    sFileHeader _Head;
    QByteArray _controlSection;
    tLinkData _ChIdxtoCID;
    QString _filename;
    QString _testName;
    QFile* _file;
    double _scanStep;
    bool _end;
    bool _scheme1;
    bool _opened;
    bool _state;
    int _direction;
    int _lastOffSet;
    int _offSet;
    int _coordinate;
    int _disCoord;
    int _statedCoord;
    int _maxDisCoord;
    int _startDist;
    int _count;
};
#endif  // FILEREADER_H
