//#ifndef TTEXTDATASOURCE_H
//#define TTEXTDATASOURCE_H
#ifndef EK_ASUI_H
#define EK_ASUI_H

#include <QVariant>
#include <QVector>
#include <QFile>
#include <QtXml>
#include <QMap>
#include <QObject>
#include "coredefinitions.h"

class EK_ASUI : public QObject
{
    Q_OBJECT

private:
    int IncidentIndex;
    QString FWorkDir;

    QMap<QString, QString> RailRoads;
    QMap<QString, QString> RailTypes;
    QMap<QString, QString> RailFabrics;
    QMap<QString, QString> Stations_CODE;

    QMap<QString, QString> attributesPath;
    QMap<QString, QString> attributesPathForXml;
    QStringList selectedRowsFromPUTfiles;
    QStringList selectedRowsFromSTRELfiles;

    // QString idPath;
    QString pathType;
    QString pathTypeForXml;
    QString railType;
    QString fabricCode;
    QString monthOnRail;

    int selectedIndex = 0;
    const int titleRow = 1;

    // Классификатор дефектов
    std::vector<int> DEF_RAid;
    std::vector<qlonglong> DEF_id;
    QStringList DEF_shortName;
    QStringList DEF_fullName;

    QString NSIversion;  // Версия нормативно справочная информации
    QString carID;       // ID средства НК

    // Загруженные данные
    QStringList load_RR;
    QStringList load_PRED;

    bool LoadRailRoads();
    void LoadRailTypes();
    void LoadRailFabrics();
    int getFieldIndex(const QString& fieldName, const QString& line);
    bool checkPREDid();


public:
    EK_ASUI(const QString& WorkDir, bool LoadAllData = true);
    QObject* parent = nullptr;
    ~EK_ASUI();
    void SetCarID(const QString& carID_);

    QStringList GetRRs();  // Получение списка Дорог

    QString getStartKmTrackMark();
    QString getStartMTrackMark();
    QString getEndKmTrackMark();
    QString getEndMTrackMark();
    QString getRailRoadCode();
    QString getPathID();
    void setSelectedItemIndex(int index);
    void setSelectedItemIndex(QString nput, QString type);
    void setSelectedIndexForSwitch(QString switchNumber);
    void SetPathType(const QString& chosenPath);
    void reSetPathType();
    void SetPathTypeSwitch();
    QString GetPathType();
    QStringList GetDirectionNamesList();
    QStringList GetStationNamesList();
    QString GetSCOD_by_Station_Name(QString);
    void addStationDataToStaionsMap(QString name, QString scod);
    QString GetIdRailRoadByName(QString railRoadName);
    QStringList GetMainPath_NPUT_List(QString direction);
    QMultiMap<QString, QString> GetStationPath_NPUT_List(QString stationName);
    QStringList GetRailTypesList();
    QStringList GetFabricsList();
    QStringList GetSwitchList();
    void SetRailType(QString selectedRailType);
    QString GetRailType();
    void SetMonth(int selectedMonth);
    QString GetMonth();
    void SetFabricCode(QString selectedFabric);
    QString GetFabricCode();
    QStringList GetDEFECTshortNames();                          // Получение коротких названий дефектов
    QStringList GetDEFECTdataBYshortName(const QString& name);  // Получение инфомации о дефекте по короткму названию

    bool CreateIncidentXML(const QString& fileName,     // Полное имя создоваемого файла
                           const QString& fwVersion,    // Версия ПО БУИ
                           const QString& decoderName,  // Оператор/расшифровщик
                           const QString& DefType,
                           QString DefCode,
                           int Rail,        // Нить пути: 0-Левая; 1-Правая
                           int DefDepth,    // Глубина дефекта
                           int speedLimit,  // Ограничение скорости
                           int DefWidth,    // Ширина дефекта
                           QString Notification,
                           int DefLength,  // Длина дефекта
                           const QString& railYear,
                           const QString& Smelting,
                           const QString& posHint,     // Привязка
                           int posKM,                  // Километр
                           int posPK,                  // Пикет
                           int posM,                   // Метр
                           void* Image_Ptr,            // Изображение дефекта - указатель на изображение в формате PNG
                           int Image_Size,             // Изображение дефекта - размер изображения
                           const QString& Image_Name,  // Имя картинки
                           double GPS_latitude,        // GPS коодината - Широта
                           double GPS_longitude);      // GPS коодината - Долгота

    bool CreateMainPathsIncidentXML(QString fileName,       // Полное имя создоваемого файла
                                    QString fwVersion,      // Версия ПО БУИ [+]
                                    QString decoderName,    // Оператор/расшифровщик [+]
                                    QString DefType,        // defect type
                                    QString DefCode,        // defect code
                                    int Rail,               // Нить пути: 0-Левая; 1-Правая [+]
                                    int DefDepth,           // Глубина дефекта [+]
                                    int speedLimit,         // Ограничение скорости [+]
                                    int DefWidth,           // Ширина дефекта [+]
                                    QString Notification,   // notification
                                    int DefLength,          // Длина дефекта [+]
                                    QString railYear,       // rail year
                                    QString Smelting,       // smelting
                                    QString posHint,        // Привязка [+]
                                    int posKM,              // Километр [+]
                                    int posPK,              // Пикет [+]
                                    int posM,               // Метр [+]
                                    void* Image_Ptr,        // Изображение дефекта - указатель на изображение в формате PNG
                                    int Image_Size,         // Изображение дефекта - размер изображения
                                    QString Image_Name,     // Имя картинки
                                    double GPS_latitude,    // GPS коодината - Широта
                                    double GPS_longitude);  // GPS коодината - Долгота

    bool CreateStationIncidentXML(QString fileName,     // Полное имя создоваемого файла
                                  QString fwVersion,    // Версия ПО БУИ [+]
                                  QString decoderName,  // Оператор/расшифровщик [+]
                                  QString DefType,
                                  QString DefCode,
                                  int Rail,        // Нить пути: 0-Левая; 1-Правая [+]
                                  int DefDepth,    // Глубина дефекта [+]
                                  int speedLimit,  // Ограничение скорости [+]
                                  int DefWidth,    // Ширина дефекта [+]
                                  QString Notification,
                                  int DefLength,          // Длина дефекта [+]
                                  QString railYear,       // rail year
                                  QString Smelting,       // smelting
                                  QString posHint,        // Привязка [+]
                                  int posKM,              // Километр [+]
                                  int posPK,              // Пикет [+]
                                  int posM,               // Метр [+]
                                  void* Image_Ptr,        // Изображение дефекта - указатель на изображение в формате PNG
                                  int Image_Size,         // Изображение дефекта - размер изображения
                                  QString Image_Name,     // Имя картинки
                                  double GPS_latitude,    // GPS коодината - Широта
                                  double GPS_longitude);  // GPS коодината - Долгота
    int getIncidentIndex();
    void resetIncidentIndex();
};

#endif  // TTEXTDATASOURCE_H
