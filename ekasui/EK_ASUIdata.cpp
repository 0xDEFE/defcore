#include "EK_ASUIdata.h"
#include "settings.h"
#include "qdebug.h"

EK_ASUI::EK_ASUI(const QString& WorkDir, bool LoadAllData)
{
    IncidentIndex = 1;

    FWorkDir = WorkDir;
    LoadRailRoads();
    LoadRailTypes();
    LoadRailFabrics();

    // Загрузка - "Классификатор дефектов"

    DEF_RAid.clear();
    DEF_id.clear();
    DEF_shortName.clear();
    DEF_fullName.clear();

    DEF_RAid.reserve(128);
    DEF_id.reserve(128);

    QFile file1(":/DEFECTs.txt");

    if (file1.open(QIODevice::ReadOnly)) {
        int line = 0;
        while (!file1.atEnd()) {
            const QString& str = file1.readLine();
            if (line == 0) {
                NSIversion = str;
            }
            else if (line > 1) {
                const QStringList& list = str.split(";");
                DEF_RAid.push_back(list[0].toInt());
                DEF_id.push_back(list[1].toLongLong());
                DEF_shortName << list[2].simplified();
                DEF_fullName << list[3].simplified();
            }
            line++;
        }
        file1.close();
    }
}

EK_ASUI::~EK_ASUI()
{
    RailRoads.clear();
    RailTypes.clear();
    RailFabrics.clear();
    Stations_CODE.clear();
    DEF_RAid.clear();
    DEF_id.clear();
    DEF_shortName.clear();
    DEF_fullName.clear();
    NSIversion.clear();
}

bool EK_ASUI::LoadRailRoads()
{
    RailRoads.clear();

    QFile file(":/DOROGI.txt");
    if (!file.open(QIODevice::ReadOnly)) {
        return false;
    }
    int line = 0;
    while (!file.atEnd()) {
        const QString& str = file.readLine();
        if (line != 0) {
            const QStringList& list = str.split(";");
            RailRoads.insert(list[1].trimmed().toUpper(), list[0]);
        }
        line++;
    }
    file.close();
    return true;
}

void EK_ASUI::SetPathType(const QString& chosenPath)
{
    pathTypeForXml = chosenPath;
    pathType = chosenPath;
}

void EK_ASUI::reSetPathType()
{
    pathTypeForXml = pathType;
    attributesPathForXml = attributesPath;
}

void EK_ASUI::SetPathTypeSwitch()
{
    pathTypeForXml = "2";
}


QString EK_ASUI::GetPathType()
{
    return pathType;
}

void EK_ASUI::SetCarID(const QString& carID_)
{
    carID = carID_;
}

bool EK_ASUI::CreateIncidentXML(const QString& fileName,     // Полное имя создоваемого файла
                                const QString& fwVersion,    // Версия ПО БУИ [+]
                                const QString& decoderName,  // Оператор/расшифровщик [+]
                                const QString& DefType,
                                QString DefCode,
                                int Rail,        // Нить пути: 0-Левая; 1-Правая [+]
                                int DefDepth,    // Глубина дефекта [+]
                                int speedLimit,  // Ограничение скорости [+]
                                int DefWidth,    // Ширина дефекта [+]
                                QString Notification,
                                int DefLength,  // Длина дефекта [+]
                                const QString& railYear,
                                const QString& Smelting,
                                const QString& posHint,  // Привязка [+]
                                int posKM,               // Километр [+]
                                int posPK,               // Пикет [+]
                                int posM,                // Метр [+]
                                void* Image_Ptr,         // Изображение дефекта - указатель на изображение в формате PNG
                                int Image_Size,          // Изображение дефекта - размер изображения
                                const QString& Image_Name,
                                double GPS_latitude,   // GPS коодината - Широта
                                double GPS_longitude)  // GPS коодината - Долгота
{
    QDateTime dt = QDateTime::currentDateTime();
    int recNo = 0;
    QDomDocument* doc;
    QDomElement docbody;
    if (QFile::exists(fileName)) {
        qDebug() << "file " << fileName << " exists";
        QFile file(fileName);
        file.open(QFile::ReadOnly | QIODevice::Text);

        doc = new QDomDocument();
        doc->setContent(file.readAll());

        docbody = doc->documentElement();
        if (docbody.tagName() != "OutFile") {
            return false;
        }

        recNo = 1;
        QDomNode node = docbody.firstChild();
        while (!node.isNull()) {
            if (node.toElement().tagName() == "incident") {
                recNo++;
            }
            node = node.nextSibling();
        }
    }
    else {
        qDebug() << "creating the new defect .xml file";
        doc = new QDomDocument(fileName);

        QString xmlstr = R"(<?xml version="1.0" encoding="UTF-8"?>)";
        doc->setContent(xmlstr, true);

        docbody = doc->createElement("Defect");

        docbody.setAttribute("Manufacture", "RDVNKA");
        docbody.setAttribute("soft", APP_VERSION);
        docbody.setAttribute("NSIver", restoreEKASUINSIVer());
        docbody.setAttribute("carID", restoreEKASUICarId());
        docbody.setAttribute("PredID", restoreEKASUIPredId());
        docbody.setAttribute("recID", dt.toString("yyyyMMddhhmmsszzz"));  // УНИКАЛЬНЫЙ НОМЕР ЗАПИСИ ОБ ИНЦИДЕНТЕ
        docbody.setAttribute("SiteID", attributesPathForXml.value("NDOR"));
        docbody.setAttribute("runTime", dt.toString("dd.MM.yyyy hh:mm:ss"));     // дата формирования файла
        docbody.setAttribute("decodeTime", dt.toString("dd.MM.yyyy hh:mm:ss"));  // ДАТА РАСШИФРОВКИ
        docbody.setAttribute("decoder", decoderName);                            // ФИО РАСШИФРОВЩИКА
        docbody.setAttribute("pathID", attributesPathForXml.value("IDPUT"));
        docbody.setAttribute("pathType", pathTypeForXml);
        docbody.setAttribute("pathText", attributesPathForXml.value("NPUT"));  // ПУТЬ
        docbody.setAttribute("thread", Rail);
        docbody.setAttribute("posKM", QString::number(posKM));              // КООРДИНАТА КМ ИНЦИДЕНТА
        docbody.setAttribute("posM", QString::number(posM + posPK * 100));  // КООРДИНАТА М ИНЦИДЕНТА
        if (DefType == "-1") {
            Notification = "";
            DefCode = "";
            docbody.setAttribute("sizeDepth", "");
            docbody.setAttribute("sizeLength", "");
            docbody.setAttribute("sizeWidth", "");
            docbody.setAttribute("speedLimitID", "");
        }
        else {
            docbody.setAttribute("sizeDepth", QString::number(DefDepth));    // ГЛУБИНА ОТСТУПЛЕНИЯ
            docbody.setAttribute("sizeLength", QString::number(DefLength));  // ДЛИНА ОСТУПЛЕНИЯ
            docbody.setAttribute("sizeWidth", QString::number(DefWidth));    // ШИРИНА ОТСТУПЛЕНИЯ
            if (speedLimit != 999) {
                docbody.setAttribute("speedLimitID", QString::number(speedLimit));  // ОГРАНИЧЕНИЕ СКОРОСТИ
            }
            else {
                docbody.setAttribute("speedLimitID", ("установленная"));
            }
        }
        docbody.setAttribute("DefType", DefType);
        docbody.setAttribute("NotificationNum", Notification);
        docbody.setAttribute("DefPic", DefCode);
        docbody.setAttribute("Fabric", fabricCode);
        docbody.setAttribute("Month", monthOnRail);
        docbody.setAttribute("Year", railYear);
        docbody.setAttribute("TypeRail", railType);
        docbody.setAttribute("SmeltingNum", Smelting);
        docbody.setAttribute("comment", posHint);
        docbody.setAttribute("lon", QString::number(GPS_longitude, 'g', 8));  // GPS - Долгота
        docbody.setAttribute("lat", QString::number(GPS_latitude, 'g', 8));   // GPS - Широта

        QByteArray Image((char*) Image_Ptr, Image_Size);
        docbody.setAttribute("Pic", QString(Image.toBase64()));

        doc->appendChild(docbody);


        QFile file(fileName);
        if (file.open(QIODevice::WriteOnly)) {
            QTextStream out(&file);
            doc->save(out, 0, QDomNode::EncodingPolicy(2));  // QDomNode::EncodingPolicy(2) - save (") as (") in file, else (')
            file.close();
            qDebug() << "xml Defect file has been created!!";
        }
        else {
            qDebug() << "xml Defect file hasn't been created!!";
        }

        attributesPathForXml = attributesPath;
        return true;
    }
}

bool EK_ASUI::CreateMainPathsIncidentXML(QString fileName,     // Полное имя создоваемого файла
                                         QString fwVersion,    // Версия ПО БУИ [+]
                                         QString decoderName,  // Оператор/расшифровщик [+]
                                         QString DefType,
                                         QString DefCode,
                                         int Rail,        // Нить пути: 0-Левая; 1-Правая [+]
                                         int DefDepth,    // Глубина дефекта [+]
                                         int speedLimit,  // Ограничение скорости [+]
                                         int DefWidth,    // Ширина дефекта [+]
                                         QString Notification,
                                         int DefLength,  // Длина дефекта [+]
                                         QString railYear,
                                         QString Smelting,
                                         QString posHint,      // Привязка [+]
                                         int posKM,            // Километр [+]
                                         int posPK,            // Пикет [+]
                                         int posM,             // Метр [+]
                                         void* Image_Ptr,      // Изображение дефекта - указатель на изображение в формате PNG
                                         int Image_Size,       // Изображение дефекта - размер изображения
                                         QString Image_Name,   // Имя картинки
                                         double GPS_latitude,  // GPS коодината - Широта
                                         double GPS_longitude  // GPS коодината - Долгота
)
{
    return CreateIncidentXML(fileName, fwVersion, decoderName, DefType, DefCode, Rail, DefDepth, speedLimit, DefWidth, Notification, DefLength, railYear, Smelting, posHint, posKM, posPK, posM, Image_Ptr, Image_Size, Image_Name, GPS_latitude, GPS_longitude);
}

bool EK_ASUI::CreateStationIncidentXML(QString fileName,     // Полное имя создоваемого файла
                                       QString fwVersion,    // Версия ПО БУИ [+]
                                       QString decoderName,  // Оператор/расшифровщик [+]
                                       QString DefType,
                                       QString DefCode,
                                       int Rail,        // Нить пути: 0-Левая; 1-Правая [+]
                                       int DefDepth,    // Глубина дефекта [+]
                                       int speedLimit,  // Ограничение скорости [+]
                                       int DefWidth,    // Ширина дефекта [+]
                                       QString Notification,
                                       int DefLength,  // Длина дефекта [+]
                                       QString railYear,
                                       QString Smelting,
                                       QString posHint,       // Привязка [+]
                                       int posKM,             // Километр [+]
                                       int posPK,             // Пикет [+]
                                       int posM,              // Метр [+]
                                       void* Image_Ptr,       // Изображение дефекта - указатель на изображение в формате PNG
                                       int Image_Size,        // Изображение дефекта - размер изображения
                                       QString Image_Name,    // Имя картинки
                                       double GPS_latitude,   // GPS коодината - Широта
                                       double GPS_longitude)  // GPS коодината - Долгота
{
    return CreateIncidentXML(fileName, fwVersion, decoderName, DefType, DefCode, Rail, DefDepth, speedLimit, DefWidth, Notification, DefLength, railYear, Smelting, posHint, posKM, posPK, posM, Image_Ptr, Image_Size, Image_Name, GPS_latitude, GPS_longitude);
}

int EK_ASUI::getIncidentIndex()
{
    return IncidentIndex++;
}

void EK_ASUI::resetIncidentIndex()
{
    IncidentIndex = 1;
}

// --------------------------------------------------------------------------

// Получение списка Дорог
QStringList EK_ASUI::GetRRs()
{
    QStringList names;
    QMap<QString, QString>::const_iterator i = RailRoads.constBegin();
    while (i != RailRoads.constEnd()) {
        names.append(i.key());
        ++i;
    }
    return names;
}

QString EK_ASUI::getStartKmTrackMark()
{
    return attributesPathForXml.value("S_KM");
}

QString EK_ASUI::getStartMTrackMark()
{
    return attributesPathForXml.value("S_M");
}

QString EK_ASUI::getEndKmTrackMark()
{
    return attributesPathForXml.value("E_KM");
}

QString EK_ASUI::getEndMTrackMark()
{
    return attributesPathForXml.value("E_M");
}

QString EK_ASUI::getRailRoadCode()
{
    return attributesPathForXml.value("NDOR");
}

QString EK_ASUI::getPathID()
{
    return attributesPathForXml.value("IDPUT");
}

QString EK_ASUI::GetIdRailRoadByName(QString railRoadName)
{
    return RailRoads.value(railRoadName);
}

// --------------------------------------------------------------------------


QStringList EK_ASUI::GetDirectionNamesList()
{
    QStringList names;
    QString line;
    QFile file(FWorkDir + "DIRECTION.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return names;
    }
    else {
        qDebug() << "open file DIRECTION.txt";
    }
    QTextStream out(&file);
    while (!out.atEnd()) {
        line = out.readLine();
        names.push_back(line);
    }
    file.close();
    return names;
}

QStringList EK_ASUI::GetStationNamesList()
{
    QStringList names, splited;
    QString line;
    QFile file(FWorkDir + "STAN_LIST.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return names;
    }
    else {
        qDebug() << "open file STAN_LIST.txt";
    }
    QTextStream out(&file);
    while (!out.atEnd()) {
        line = out.readLine();
        splited = line.split(";");
        names.push_back(splited[0]);
        addStationDataToStaionsMap(splited[0], splited[1]);
    }
    file.close();
    return names;
}

void EK_ASUI::addStationDataToStaionsMap(QString name, QString scod)
{
    Stations_CODE.insert(name, scod);
}

QString EK_ASUI::GetSCOD_by_Station_Name(QString name)
{
    return Stations_CODE.value(name);
}

void EK_ASUI::setSelectedItemIndex(int index)
{
    selectedIndex = index;
    QString keysLine, valuesLine;
    QStringList keys, values;
    int titleLine = 0;
    keysLine = selectedRowsFromPUTfiles[titleLine];
    keys = keysLine.split(";");
    valuesLine = selectedRowsFromPUTfiles[++index];
    values = valuesLine.split(";");
    for (int k = 0; k < keys.size(); ++k) {
        attributesPath.insert(keys[k], values[k]);
    }
    attributesPathForXml = attributesPath;
}

void EK_ASUI::setSelectedItemIndex(QString nput, QString type)
{
    QString keysLine;
    QStringList keys, values;
    int titleLine = 0;
    keysLine = selectedRowsFromPUTfiles[titleLine];
    keys = keysLine.split(";");
    for (int i = 0; i < selectedRowsFromPUTfiles.size(); ++i) {
        if (selectedRowsFromPUTfiles.at(i).contains(nput) && selectedRowsFromPUTfiles.at(i).contains(type)) {
            values = selectedRowsFromPUTfiles.at(i).split(";");
            break;
        }
    }
    for (int k = 0; k < keys.size(); ++k) {
        attributesPath.insert(keys[k], values[k]);
    }
    attributesPathForXml = attributesPath;
}

void EK_ASUI::setSelectedIndexForSwitch(QString switchNumber)
{
    QString keysLine;
    QStringList keys, values;
    QMap<QString, QString> switchAttributes;
    QString idPUT = attributesPath.value("IDPUT");

    int titleLine = 0;
    keysLine = selectedRowsFromSTRELfiles[titleLine];
    keys = keysLine.split(";");
    for (int i = 0; i < selectedRowsFromSTRELfiles.size(); ++i) {
        if (selectedRowsFromSTRELfiles.at(i).contains(";" + switchNumber + ";") && selectedRowsFromSTRELfiles.at(i).contains(idPUT)) {
            values = selectedRowsFromSTRELfiles.at(i).split(";");
            break;
        }
    }
    for (int k = 0; k < keys.size(); ++k) {
        switchAttributes.insert(keys[k], values[k]);
    }
    attributesPathForXml["IDPUT"] = switchAttributes.value("STRELID");
    attributesPathForXml["NPUT"] = switchAttributes.value("NUM");
}

void EK_ASUI::LoadRailTypes()
{
    QStringList splited;
    QString line;
    QFile file(":/RAILTYPE.txt");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream out(&file);
        line = out.readLine();
        while (!out.atEnd()) {
            line = out.readLine();
            splited = line.split(";");
            RailTypes.insert(splited[0].trimmed(), splited[1]);
        }
    }
    else {
        qDebug() << "cant open file RAILTYPE.txt";
    }
}

QStringList EK_ASUI::GetRailTypesList()
{
    QStringList railTypes;
    QMap<QString, QString>::const_iterator i = RailTypes.constBegin();
    while (i != RailTypes.constEnd()) {
        railTypes.append(i.value());
        ++i;
    }
    return railTypes;
}

void EK_ASUI::LoadRailFabrics()
{
    QStringList splited;
    QString line;
    QFile file(":/FACTORY.txt");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream out(&file);
        line = out.readLine();
        while (!out.atEnd()) {
            line = out.readLine();
            splited = line.split(";");
            RailFabrics.insert(splited[0].trimmed(), splited[1]);
        }
    }
    else {
        qDebug() << "cant open file FACTORY.txt";
    }
}


QStringList EK_ASUI::GetFabricsList()
{
    QStringList fabrics;
    QMap<QString, QString>::const_iterator i = RailFabrics.constBegin();
    while (i != RailFabrics.constEnd()) {
        fabrics.append(i.value());
        ++i;
    }
    return fabrics;
}

QStringList EK_ASUI::GetSwitchList()
{
    QStringList rrSwitches, splitedStr;
    QString line;
    int index = -1;
    selectedRowsFromSTRELfiles.clear();
    rrSwitches.push_back("-");
    QFile file(FWorkDir + "STREL.txt");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream out(&file);
        line = out.readLine();
        selectedRowsFromSTRELfiles.append(line);
        splitedStr = line.split(";");
        for (int i = 0; i < splitedStr.size(); ++i) {
            if (splitedStr[i] == "NUM") {
                index = i;
            }
        }
        while (!out.atEnd()) {
            line = out.readLine();
            if (line.contains(attributesPathForXml.value("IDPUT"))) {
                selectedRowsFromSTRELfiles.append(line);
                splitedStr = line.split(";");
                rrSwitches.push_back(splitedStr[index]);
            }
        }
        rrSwitches.sort();
    }
    else {
        qDebug() << "cant open file STREL.txt";
    }
    return rrSwitches;
}

void EK_ASUI::SetRailType(QString selectedRailType)
{
    railType = RailTypes.key(selectedRailType);
}

QString EK_ASUI::GetRailType()
{
    return railType;
}

void EK_ASUI::SetMonth(int selectedMonth)
{
    monthOnRail = QString::number(selectedMonth);
}

QString EK_ASUI::GetMonth()
{
    return monthOnRail;
}

void EK_ASUI::SetFabricCode(QString selectedFabric)
{
    fabricCode = RailFabrics.key(selectedFabric);
}

QString EK_ASUI::GetFabricCode()
{
    return fabricCode;
}

QStringList EK_ASUI::GetMainPath_NPUT_List(QString direction)
{
    QStringList paths, splited;
    QString line;
    int index = 0;
    int titleLine = 0;
    selectedRowsFromPUTfiles.clear();
    QFile file(FWorkDir + "PUT_GL.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return paths;
    }
    else {
        qDebug() << "open file PUT_GL.txt";
    }
    QTextStream out(&file);
    line = out.readLine();
    selectedRowsFromPUTfiles.append(line);
    while (!out.atEnd()) {
        line = out.readLine();
        if (line.contains(direction)) {
            selectedRowsFromPUTfiles.append(line);
        }
    }
    file.close();
    splited = selectedRowsFromPUTfiles[titleLine].split(";");
    for (int i = 0; i < splited.size(); ++i) {
        if (splited[i] == "NPUT") {
            index = i;
        }
    }
    for (int j = 1; j < selectedRowsFromPUTfiles.size(); ++j) {
        splited = selectedRowsFromPUTfiles[j].split(";");
        paths.push_back(splited[index]);
    }
    return paths;
}

QMultiMap<QString, QString> EK_ASUI::GetStationPath_NPUT_List(QString stationName)
{
    QStringList splited;
    QMultiMap<QString, QString> stanPaths;
    QString line, scod;
    int indexNPUT = 0;
    int indexTYP = 0;
    selectedRowsFromPUTfiles.clear();
    scod = GetSCOD_by_Station_Name(stationName);
    QFile file(FWorkDir + "PUT_ST.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return stanPaths;
    }
    else {
        qDebug() << "open file PUT_ST.txt";
    }
    QTextStream out(&file);
    line = out.readLine();
    selectedRowsFromPUTfiles.append(line);
    while (!out.atEnd()) {
        line = out.readLine();
        if (line.contains(scod)) {
            selectedRowsFromPUTfiles.append(line);
        }
    }
    file.close();
    splited = selectedRowsFromPUTfiles[0].split(";");
    for (int i = 0; i < splited.size(); ++i) {
        if (splited[i] == "NPUT") {
            indexNPUT = i;
        }
        if (splited[i] == "TYP") {
            indexTYP = i;
        }
    }
    for (int j = 1; j < selectedRowsFromPUTfiles.size(); ++j) {
        splited = selectedRowsFromPUTfiles[j].split(";");
        stanPaths.insert(splited[indexTYP], splited[indexNPUT]);
    }
    return stanPaths;
}


// --------------------------------------------------------------------------

QStringList EK_ASUI::GetDEFECTshortNames()  // Получение коротких названий дефектов
{
    return DEF_shortName;
    /*
        QStringList list;

        for (unsigned int idx = 0; idx < DEF_shortName.size(); idx++)
            list.append("ra_id: " + QString::number(DEF_RAid[idx]) +
                        " EKASUI_id: " + QString::number(DEF_id[idx]) +
                        " " + DEF_shortName[idx] +
                        " " + DEF_fullName[idx]);
        return list;
    */
}

QStringList EK_ASUI::GetDEFECTdataBYshortName(const QString& name)  // Получение инфомации о дефекте по короткму названию
{
    QStringList list;
    int idx = DEF_shortName.indexOf(name);
    if (idx != -1) {
        list.append(QString::number(DEF_RAid[idx]));
        list.append(QString::number(DEF_id[idx]));
        list.append(DEF_fullName[idx]);
    }
    return list;
}
