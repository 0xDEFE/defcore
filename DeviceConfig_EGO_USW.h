#ifndef DEVICECONFIG_EGO_USW_H
#define DEVICECONFIG_EGO_USW_H

#include "DeviceConfig.h"

class cDeviceConfig_EGO_USW : public cDeviceConfig
{
private:
    unsigned char UMUGain[81];

public:
    cDeviceConfig_EGO_USW(cChannelsTable* channelsTable, int aScanThreshold, int bScanThreshold, eCoordSys type);
    ~cDeviceConfig_EGO_USW(void);
    unsigned char dBGain_to_UMUGain(char dBGain);
};

#endif /* DEVICECONFIG_EGO_USW_H */
